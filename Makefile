IMG=helloworld.img
SCRS=helloworld.scr \
	${PREFIX}/visable-forth/gui/gui-input.scr

BLKS+=${SCRS:M*.scr:R:S/$/.blk/}

.include "config.mk"

.SUFFIXES: .scr .blk
.scr.blk:
	${SCR} ${SCRFLAGS} ${.IMPSRC} ${.TARGET}

.PHONY: build clean

build: ${IMG}

clean:
	-rm -vf ${IMG}
	-rm -vf *.blk

${IMG}: ${PREFIX}/visable-forth/visable.img ${BLKS}
	cp ${PREFIX}/visable-forth/visable.img ${IMG}
	${BLKLD} ${BLKLDFLAGS} -o ${IMG} ${BLKS}
